<#import "layout.ftl" as core>
<@core.layout title=title>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="card neumorphism mb-3" style="margin: auto; max-width: 500px;">
                    <div class="user text-center">
                        <div class="profile neumorphism">
                            <img src="${user.avatarUrl}" class="rounded-circle" width="80">
                        </div>
                    </div>
                    <div class="text-center">
                        <h4 class="mb-0">${user.login}</h4>
                        <span class="text-muted d-block mb-2">${user.location}</span>
                        <button class="btn btn-outline-dark btn-sm follow neumorphism">Refresh</button>
                        <div class="d-flex justify-content-between align-items-center mt-4 p-4">
                            <div class="stats">
                                <h6 class="mb-0">Followers</h6>
                                <span>${user.followers}</span>
                            </div>
                            <div class="stats">
                                <h6 class="mb-0">Projects</h6>
                                <span>${user.publicRepos}</span>
                            </div>
                            <div class="stats">
                                <h6 class="mb-0">Ranks</h6>
                                <span>${user.id}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="card neumorphism" style="padding:unset; margin: auto; max-width: 500px;">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h6>Repositories</h6>
                            <button class="btn btn-outline-dark neumorphism">Refresh</button>
                        </div>
                    </div>
                    <div class="card-body"></div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card neumorphism">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <h6>Followers</h6>
                        <a href="/profile/${user.login}/refresh-followers" class="btn btn-outline-dark neumorphism">
                            Refresh
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive" style="max-height: 500px; overflow-x: hidden;">
                        <table class="table table-borderless table-hover table-sm" data-height="460">
                            <thead>
                            <tr>
                                <th style="width: 50px;"></th>
                                <th style="width: 100px; vertical-align: middle; text-align: center;">
                                    ID
                                </th>
                                <th style="width: 75px; vertical-align: middle; text-align: center;">
                                    Login
                                </th>
                                <th style="width: 150px;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list followers as user>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <img class="avatar-sm-rounded neumorphism"
                                             src="${user.avatarUrl}"
                                             alt="avatar">
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        ${user.id}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        ${user.login}
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <div class="d-md-flex gap-2 justify-content-center">
                                            <a href="/profile/${user.login}"
                                               class="btn btn-info neumorphism"
                                               data-bs-toggle="tooltip"
                                               data-bs-placement="top"
                                               title="User's info">
                                                <i class="bi bi-info"></i>
                                            </a>
                                            <a href="#"
                                               class="btn btn-light neumorphism"
                                               data-bs-toggle="tooltip"
                                               data-bs-placement="top"
                                               title="User's info download">
                                                <i class="bi bi-cloud-download"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@core.layout>