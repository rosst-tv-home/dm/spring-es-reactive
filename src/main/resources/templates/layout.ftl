<#macro layout title="">
    <!DOCTYPE html>
    <html lang="en" style="margin: unset; height: 100%;">
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <link rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
        <style>
            :root {
                --bg: #e6e7ee;
                --br: #d1d9e6;
            }

            html, body {
                height: 100%;
                margin: unset;
            }

            body {
                display: flex;
                align-items: center;
                background-color: var(--bg);
            }

            .avatar-sm-rounded {
                width: 50px;
                height: 50px;
                border-radius: .75rem;
            }

            .neumorphism {
                border: 1px solid var(--br) !important;
                border-radius: .75rem !important;
                box-shadow: 9px 12px 15px rgba(184, 185, 190, .3), -9px -12px 15px rgba(255, 255, 255, .3) !important;
                transition: all 0.2s ease-in-out;
            }

            .neumorphism:hover {
                box-shadow: 2px 2px 5px rgb(184, 185, 190), -2px -2px 5px rgb(255, 255, 255) !important;
            }

            .profile {
                width: 80px;
                border-radius: 50% !important;
                position: relative;
                top: -50px;
                margin: auto;
            }
        </style>
    </head>
    <body>
    <div class="container">
        <#nested>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
            integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
            integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
            crossorigin="anonymous"></script>
    </body>
    </html>
</#macro>