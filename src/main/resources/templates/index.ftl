<#import "layout.ftl" as core>
<@core.layout title=title>
    <div class="row">
        <div class="col">
            <div class="card neumorphism">
                <div class="card-header">
                    <ul class="nav nav-pills justify-content-around">
                        <li class="nav-item neumorphism">
                            <a class="nav-link ${byIdIsActive}"
                               href="http://localhost:81/by-id?ascending=${byIdAscending}">
                                By ID
                                <#if byIdIsActive == "active">
                                    <#if byIdAscending == "true">
                                        <i class="bi bi-arrow-up-short"></i>
                                    <#else>
                                        <i class="bi bi-arrow-down-short"></i>
                                    </#if>
                                </#if>
                            </a>
                        </li>
                        <li class="nav-item neumorphism">
                            <a class="nav-link ${byLoginIsActive}"
                               href="http://localhost:81/by-login?ascending=${byLoginAscending}">
                                By Login
                                <#if byLoginIsActive == "active">
                                    <#if byLoginAscending == "true">
                                        <i class="bi bi-arrow-up-short"></i>
                                    <#else>
                                        <i class="bi bi-arrow-down-short"></i>
                                    </#if>
                                </#if>
                            </a>
                        </li>
                        <li class="nav-item neumorphism">
                            <a class="nav-link ${byReposIsActive}"
                               href="http://localhost:81/by-repos?ascending=${byReposAscending}">
                                By Repos
                                <#if byReposIsActive == "active">
                                    <#if byReposAscending == "true">
                                        <i class="bi bi-arrow-up-short"></i>
                                    <#else>
                                        <i class="bi bi-arrow-down-short"></i>
                                    </#if>
                                </#if>
                            </a>
                        </li>
                        <li class="nav-item neumorphism">
                            <a class="nav-link ${byFollowersIsActive}"
                               href="http://localhost:81/by-followers?ascending=${byFollowersAscending}">
                                By Followers
                                <#if byFollowersIsActive == "active">
                                    <#if byFollowersAscending == "true">
                                        <i class="bi bi-arrow-up-short"></i>
                                    <#else>
                                        <i class="bi bi-arrow-down-short"></i>
                                    </#if>
                                </#if>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="table-responsive" style="max-height: 500px; overflow-x: hidden;">
                        <table class="table table-borderless table-hover table-sm" data-height="460">
                            <thead>
                            <tr>
                                <th style="width: 50px;"></th>
                                <th style="width: 100px; vertical-align: middle; text-align: center;">
                                    User's id
                                </th>
                                <th style="width: 75px; vertical-align: middle; text-align: center;">
                                    User's login
                                </th>
                                <th style="width: 150px; vertical-align: middle; text-align: center;">
                                    User's repos
                                </th>
                                <th style="width: 150px; vertical-align: middle; text-align: center;">
                                    User's followers
                                </th>
                                <th style="width: 150px;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <#list users as user>
                                <tr>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <img class="avatar-sm-rounded neumorphism"
                                             src="${user.avatarUrl}"
                                             alt="avatar">
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        ${user.id}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        ${user.login}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        ${user.publicRepos}
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        ${user.followers}
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <div class="d-md-flex gap-2 justify-content-center">
                                            <a href="/profile/${user.login}"
                                               class="btn btn-info neumorphism"
                                               data-bs-toggle="tooltip"
                                               data-bs-placement="top"
                                               title="User's info">
                                                <i class="bi bi-info"></i>
                                            </a>
                                            <a href="#"
                                               class="btn btn-light neumorphism"
                                               data-bs-toggle="tooltip"
                                               data-bs-placement="top"
                                               title="User's info download">
                                                <i class="bi bi-cloud-download"></i>
                                            </a>
                                            <a href="/${user.id}"
                                               class="btn btn-danger neumorphism"
                                               data-bs-toggle="tooltip"
                                               data-bs-placement="top"
                                               title="Delete user">
                                                <i class="bi bi-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-grid">
                        <a href="http://localhost:81/fetch-users" class="btn btn-light neumorphism">
                            Fetch users
                            <i class="bi bi-cloud-download"></i>
                        </a>
                        <a href="http://localhost:81/fetch-evan-you" class="btn btn-light neumorphism">
                            Evan You
                            <i class="bi bi-cloud-download"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@core.layout>
