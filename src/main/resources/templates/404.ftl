<#import "layout.ftl" as core>
<@core.layout title='Page not found'>
    <h1 class="text">404</h1>
    <h3 class="text">Page not found</h3>
</@core.layout>