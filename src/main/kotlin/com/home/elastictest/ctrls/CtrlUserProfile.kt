package com.home.elastictest.ctrls

import com.home.elastictest.entities.User
import com.home.elastictest.rest.ReactiveClient
import com.home.elastictest.services.ServiceUsers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.reactor.awaitSingle
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import reactor.core.publisher.Mono

@Controller
@RequestMapping("/profile")
class CtrlUserProfile {
    @Autowired
    private lateinit var srvUsers: ServiceUsers

    @Autowired
    private lateinit var rxClient: ReactiveClient

    @GetMapping("/{user}")
    suspend fun userProfile(@PathVariable("user") login: String, model: Model): String {
        val user = srvUsers.findByLogin(login) ?: return "404"
        val followers = srvUsers.findUserFollowers(user.id).doOnNext { println(it) }

        // Set page title
        model["title"] = "${user.login} profile"

        // Set user
        model["user"] = user

        // Set user's followers
        model["followers"] = followers

        return "profile"
    }

    @GetMapping("/{user}/refresh-followers")
    suspend fun refreshUserFollowers(@PathVariable("user") login: String): String {
        val user = srvUsers.findByLogin(login) ?: return "redirect:/profile/${login}"

        return rxClient
            .getUserFollowers(login)
            .flatMapIterable { it }
            .doOnNext { it.ownerId = user.id }
            .log()
            .flatMap { srvUsers.saveFollower(it.toDBO()) }
            .log()
            .then(Mono.just("redirect:/profile/${login}"))
            .awaitSingle()
    }
}