package com.home.elastictest.ctrls

import com.home.elastictest.entities.User
import com.home.elastictest.rest.ReactiveClient
import com.home.elastictest.services.ServiceUsers
import com.home.elastictest.vm.UserVM
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.reactor.awaitSingle
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations
import org.springframework.data.elasticsearch.core.query.Query
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.util.*

/**
 * Controller for handling user's operations
 *
 * @property rxOps [ReactiveElasticsearchOperations] reactive data-base client
 * @property rxClient [ReactiveClient] reactive retrofit client
 */
@Controller
class CtrlUserView {
    @Autowired
    private lateinit var rxOps: ReactiveElasticsearchOperations

    @Autowired
    private lateinit var srvUsers: ServiceUsers

    @Autowired
    private lateinit var rxClient: ReactiveClient

    /**
     * GET handler of "/" endpoint - index view
     */
    @GetMapping("/")
    suspend fun index(model: Model): String = "redirect:/by-id?ascending=false"

    @GetMapping("/by-id")
    suspend fun orderUsersById(
        @RequestParam("ascending") isAscending: Boolean,
        model: Model
    ): String {
        // Set page title
        model["title"] = "Users ordered by id"

        // Set users
        model["users"] = srvUsers
            .findAll()
            .toList()
            .sortedWith { user1, user2 ->
                if (isAscending) user2.id.compareTo(user1.id)
                else user1.id.compareTo(user2.id)
            }
            .asFlow()

        model["byIdIsActive"] = "active"
        model["byIdAscending"] = (!isAscending).toString()
        model["byLoginIsActive"] = ""
        model["byLoginAscending"] = false.toString()
        model["byReposIsActive"] = ""
        model["byReposAscending"] = false.toString()
        model["byFollowersIsActive"] = ""
        model["byFollowersAscending"] = false.toString()

        return "index"
    }

    @GetMapping("/by-login")
    suspend fun orderUsersByLogin(
        @RequestParam("ascending") isAscending: Boolean,
        model: Model
    ): String {
        // Set page title
        model["title"] = "Users ordered by login"

        // Set users
        model["users"] = srvUsers
            .findAll()
            .toList()
            .sortedWith { user1, user2 ->
                if (isAscending) user2.login
                    .lowercase(Locale.getDefault())
                    .compareTo(user1.login.lowercase(Locale.getDefault()))
                else user1.login
                    .lowercase(Locale.getDefault())
                    .compareTo(user2.login.lowercase(Locale.getDefault()))
            }
            .asFlow()

        model["byIdIsActive"] = ""
        model["byIdAscending"] = false.toString()
        model["byLoginIsActive"] = "active"
        model["byLoginAscending"] = (!isAscending).toString()
        model["byReposIsActive"] = ""
        model["byReposAscending"] = false.toString()
        model["byFollowersIsActive"] = ""
        model["byFollowersAscending"] = false.toString()

        return "index"
    }

    @GetMapping("/by-repos")
    suspend fun orderUsersByRepos(
        @RequestParam("ascending") isAscending: Boolean,
        model: Model
    ): String {
        // Set page title
        model["title"] = "Users ordered by repos count"

        // Set users
        model["users"] = srvUsers
            .findAll()
            .toList()
            .sortedWith { user1, user2 ->
                if (isAscending) user2.publicRepos.compareTo(user1.publicRepos)
                else user1.publicRepos.compareTo(user2.publicRepos)
            }
            .asFlow()

        model["byIdIsActive"] = ""
        model["byIdAscending"] = false.toString()
        model["byLoginIsActive"] = ""
        model["byLoginAscending"] = false.toString()
        model["byReposIsActive"] = "active"
        model["byReposAscending"] = (!isAscending).toString()
        model["byFollowersIsActive"] = ""
        model["byFollowersAscending"] = false.toString()

        return "index"
    }

    @GetMapping("/by-followers")
    suspend fun orderUsersByFollowers(
        @RequestParam("ascending") isAscending: Boolean,
        model: Model
    ): String {
        // Set page title
        model["title"] = "Users ordered by followers count"

        // Set users
        model["users"] = srvUsers
            .findAll()
            .toList()
            .sortedWith { user1, user2 ->
                if (isAscending) user2.followers.compareTo(user1.followers)
                else user1.followers.compareTo(user2.followers)
            }
            .asFlow()

        model["byIdIsActive"] = ""
        model["byIdAscending"] = false.toString()
        model["byLoginIsActive"] = ""
        model["byLoginAscending"] = false.toString()
        model["byReposIsActive"] = ""
        model["byReposAscending"] = false.toString()
        model["byFollowersIsActive"] = "active"
        model["byFollowersAscending"] = (!isAscending).toString()

        return "index"
    }



    /**
     * GET handler of "/{id}" endpoint - remove user by id
     */
    @GetMapping("/{id}")
    fun delete(@PathVariable("id") id: Int) = rxOps
        .delete(id.toString(), rxOps.getIndexCoordinatesFor(User::class.java))
        .thenReturn("redirect:/")


    /**
     * POST handler of "/add" endpoint - add or update user
     */
    @PostMapping("/add")
    fun add(model: Model, @ModelAttribute("user") user: UserVM) = rxOps
        .save(user.toDBO(), rxOps.getIndexCoordinatesFor(User::class.java))
        .thenReturn("redirect:/")

    /**
     * POST handler of "/fetch-users" endpoint - rx request to github users, save to es db and redirect to home page
     */
    @GetMapping("/fetch-users")
    suspend fun fetchUsers() = rxClient
        .findAllUsers(rxOps
            .search(
                Query.findAll().setPageable(Pageable.unpaged()),
                User::class.java,
                rxOps.getIndexCoordinatesFor(User::class.java)
            )
            .sort { hit1, hit2 -> hit2.content.id - hit1.content.id }
            .take(1)
            .map { it.content.id }
            .defaultIfEmpty(0)
            .awaitSingle()
        )
        .flatMapIterable { it }
        .flatMap { rxOps.save(it.toDBO(), rxOps.getIndexCoordinatesFor(User::class.java)) }
        .then(Mono.just("redirect:/"))
        .awaitSingle()

    @GetMapping("/fetch-evan-you")
    suspend fun fetchEvanYou(): String {
        val user = rxClient
            .findUserByName("yyx990803")

        rxOps
            .save(user.awaitSingle().toDBO(), rxOps.getIndexCoordinatesFor(User::class.java))
            .awaitSingle()

        return "redirect:/"
    }
}