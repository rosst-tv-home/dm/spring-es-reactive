package com.home.elastictest.ctrls

import com.home.elastictest.entities.User
import com.home.elastictest.rest.ReactiveClient
import org.elasticsearch.index.query.QueryBuilders
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder
import org.springframework.data.elasticsearch.core.query.Query
import org.springframework.web.bind.annotation.*
import reactor.kotlin.core.publisher.toMono

@RestController
@RequestMapping("/rest")
class CtrlUserOperations {
    @Autowired
    private lateinit var rxOps: ReactiveElasticsearchOperations

    @Autowired
    private lateinit var rxClient: ReactiveClient

    /**
     * GET handler for "/users/ops" endpoint - find all users
     */
    @GetMapping
    fun findAll() = rxOps
        .getIndexCoordinatesFor(User::class.java)
        .toMono()
        .flatMapMany { rxOps.search(Query.findAll(), User::class.java, it) }

    /**
     * GET handler for "/users/ops/{login}" endpoint - search login naming match users
     */
    @GetMapping("/{login}")
    fun findByLogin(@PathVariable("login") login: String) = rxOps
        .getIndexCoordinatesFor(User::class.java)
        .toMono()
        .flatMapMany {
            rxOps.search(
                NativeSearchQueryBuilder().withQuery(
                    QueryBuilders
                        .matchQuery("login", login)
                ).build(),
                User::class.java,
                it
            )
        }

    /**
     * POST handler for "/users/ops" endpoint - add or update user
     */
    @PostMapping
    fun addUser(@RequestBody user: User) = rxOps
        .getIndexCoordinatesFor(User::class.java)
        .toMono()
        .flatMap { rxOps.save(user, it) }

    /**
     * DELETE handler for "/users/ops/{id}" endpoint - remove user by id
     */
    @RequestMapping(method = [RequestMethod.DELETE], path = ["/{id}"])
    fun deleteUser(@PathVariable("id") id: Int) = rxOps
        .getIndexCoordinatesFor(User::class.java)
        .toMono()
        .flatMap { rxOps.delete(id.toString(), it) }
}