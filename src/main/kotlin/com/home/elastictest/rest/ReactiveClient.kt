package com.home.elastictest.rest

import com.home.elastictest.vm.FollowerVM
import com.home.elastictest.vm.UserVM
import reactor.core.publisher.Mono
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ReactiveClient {
    @GET("/users")
    fun findAllUsers(@Query("since") since: Int = 0): Mono<List<UserVM>>

    @GET("/users/{username}")
    fun findUserByName(@Path("username") userName: String): Mono<UserVM>

    @GET("/users/{username}/followers")
    fun getUserFollowers(@Path("username") userName: String): Mono<List<FollowerVM>>

    @GET("/users/{username}/repos")
    fun getUserRepositories(@Path("username") userName: String)
}