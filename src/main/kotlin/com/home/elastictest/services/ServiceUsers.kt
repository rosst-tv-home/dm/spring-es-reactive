package com.home.elastictest.services

import com.home.elastictest.entities.Follower
import com.home.elastictest.entities.User
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.elasticsearch.index.query.QueryBuilders
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder
import org.springframework.data.elasticsearch.core.query.Query
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

/**
 * Service to interact with user's data in elasticsearch
 *
 * @property rxOps [ReactiveElasticsearchOperations] reactive elasticsearch client
 */
@Service
class ServiceUsers {
    @Autowired
    private lateinit var rxOps: ReactiveElasticsearchOperations

    /**
     * Find all users in elasticsearch
     */
    fun findAll() = rxOps.search(
        Query.findAll().setPageable(Pageable.unpaged()),
        User::class.java,
        rxOps.getIndexCoordinatesFor(User::class.java)
    ).asFlow().map { it.content }

    /**
     * Find user by login in elasticsearch
     */
    suspend fun findByLogin(login: String): User? = rxOps.search(
        NativeSearchQueryBuilder().withQuery(
            QueryBuilders.matchQuery("login", login)
        ).build(),
        User::class.java,
        rxOps.getIndexCoordinatesFor(User::class.java)
    ).awaitFirstOrNull()?.content

    /**
     * Find user's followers
     */
    fun findUserFollowers(id: Int) = rxOps.search(
        NativeSearchQueryBuilder().withQuery(QueryBuilders.matchQuery("owner_id", id)).build(),
        Follower::class.java,
        rxOps.getIndexCoordinatesFor(Follower::class.java)
    ).map { it.content }

    fun saveFollower(follower: Mono<Follower>) = rxOps.save(follower, rxOps.getIndexCoordinatesFor(Follower::class.java))
}