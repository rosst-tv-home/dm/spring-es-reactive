package com.home.elastictest.configurations

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.reactive.config.ViewResolverRegistry
import org.springframework.web.reactive.config.WebFluxConfigurer
import org.springframework.web.reactive.result.view.freemarker.FreeMarkerConfigurer
import org.springframework.web.reactive.result.view.freemarker.FreeMarkerViewResolver

/**
 * FreeMarker template engine configuration
 */
@Configuration
@EnableWebFlux
class ConfigurationWeb : WebFluxConfigurer {
    /**
     * Define view resolver as FreeMarker
     */
    override fun configureViewResolvers(registry: ViewResolverRegistry) {
        registry.freeMarker()
    }

    /**
     * Configure FreeMarker template engine
     */
    @Bean
    fun freeMarkerConfigurer() = FreeMarkerConfigurer().apply {
        // Set path where view contains
        setTemplateLoaderPath("classpath:/templates")
    }
}