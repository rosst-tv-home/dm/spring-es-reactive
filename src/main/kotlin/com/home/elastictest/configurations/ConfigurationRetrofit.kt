package com.home.elastictest.configurations

import com.fasterxml.jackson.databind.ObjectMapper
import com.home.elastictest.rest.ReactiveClient
import com.jakewharton.retrofit2.adapter.reactor.ReactorCallAdapterFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory


/**
 * Retrofit reactive client configuration
 */
@Configuration
class ConfigurationRetrofit {
    /**
     *  Configure retrofit reactive client
     */
    @Bean
    fun rxClient(): ReactiveClient = Retrofit
        .Builder()
        .baseUrl("https://api.github.com/")
        .addConverterFactory(JacksonConverterFactory.create(ObjectMapper().findAndRegisterModules()))
        .addCallAdapterFactory(ReactorCallAdapterFactory.create())
        .build()
        .create(ReactiveClient::class.java)
}