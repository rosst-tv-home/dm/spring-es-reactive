package com.home.elastictest.configurations

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.data.elasticsearch.client.ClientConfiguration
import org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient
import org.springframework.data.elasticsearch.client.reactive.ReactiveRestClients
import org.springframework.data.elasticsearch.config.AbstractReactiveElasticsearchConfiguration
import org.springframework.data.elasticsearch.repository.config.EnableReactiveElasticsearchRepositories

/**
 * Elasticsearch reactive client configuration
 *
 * @property username [String] user name for data-base connection
 * @property password [String] password for data-base connection
 * @property connectionURL [String] connection url to data-base
 */
@Configuration
@EnableReactiveElasticsearchRepositories
class ConfigurationElasticSearch : AbstractReactiveElasticsearchConfiguration() {
    @Value("\${spring.data.elasticsearch.client.reactive.username}")
    private lateinit var username: String

    @Value("\${spring.data.elasticsearch.client.reactive.password}")
    private lateinit var password: String

    @Value("\${spring.data.elasticsearch.client.reactive.endpoints}")
    private lateinit var connectionURL: String

    /**
     * Configure reactive data-base client
     */
    override fun reactiveElasticsearchClient(): ReactiveElasticsearchClient = ReactiveRestClients.create(
        ClientConfiguration
            .builder()
            .connectedTo(connectionURL)
            .withBasicAuth(username, password)
            .build()
    )
}