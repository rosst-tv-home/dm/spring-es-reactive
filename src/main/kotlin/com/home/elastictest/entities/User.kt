package com.home.elastictest.entities

import com.home.elastictest.vm.UserVM
import org.springframework.data.annotation.Id
import org.springframework.data.elasticsearch.annotations.Document
import org.springframework.data.elasticsearch.annotations.Field
import reactor.kotlin.core.publisher.toMono

/**
 * User DBO in "users" Elasticsearch index
 *
 * @param id [Int] user's id
 * @param nodeId [String] user's node id
 * @param login [String] user's login
 * @param email [String] user's email
 * @param type [String] user's type
 * @param url [String] user's url
 * @param avatarUrl [String] user's avatar url
 * @param name [String] user's name
 * @param location [String] user's location
 * @param publicRepos [Int] count of user's public repos
 * @param followers [Int] count of user's followers
 */
@Document(indexName = "users")
data class User(
    @Id
    val id: Int,
    @Field(name = "node_id")
    val nodeId: String,
    val login: String,
    val email: String?,
    val type: String,
    val url: String,
    @Field(name = "avatar_url")
    val avatarUrl: String,
    val name: String?,
    val location: String?,
    @Field(name = "public_repos")
    val publicRepos: Int,
    val followers: Int
) {
    /**
     * Convert User::class to UserVM::class
     */
    internal fun toVM() = UserVM(
        id,
        nodeId,
        login,
        email,
        type,
        url,
        avatarUrl,
        name,
        location,
        publicRepos,
        followers
    ).toMono()
}