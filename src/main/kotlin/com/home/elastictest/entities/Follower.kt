package com.home.elastictest.entities

import org.springframework.data.annotation.Id
import org.springframework.data.elasticsearch.annotations.Document
import org.springframework.data.elasticsearch.annotations.Field

@Document(indexName = "followers")
data class Follower(
    @Id
    val id: Int,
    @Field(name = "node_id")
    val nodeId: String,
    val login: String,
    @Field(name = "avatar_url")
    val avatarUrl: String,
    @Field(name = "owner_id")
    val ownerId: Int
)