package com.home.elastictest.vm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.home.elastictest.entities.User
import reactor.kotlin.core.publisher.toMono

/**
 * User VM
 *
 * @param id [Int] user's id
 * @param nodeId [String] user's node id
 * @param login [String] user's login
 * @param email [String] user's email
 * @param type [String] user's type
 * @param url [String] user's url
 * @param avatarUrl [String] user's avatar url
 * @param name [String] user's name
 * @param location [String] user's location
 * @param publicRepos [Int] count of user's public repos
 * @param followers [Int] count of user's followers
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class UserVM(
    val id: Int,
    @JsonProperty("node_id")
    val nodeId: String,
    val login: String,
    val email: String?,
    val type: String,
    val url: String,
    @JsonProperty("avatar_url")
    val avatarUrl: String,
    val name: String?,
    val location: String?,
    @JsonProperty("public_repos")
    val publicRepos: Int,
    val followers: Int
) {
    /**
     * Convert UserVM::class to User::class
     */
    internal fun toDBO() = User(
        id,
        nodeId,
        login,
        email,
        type,
        url,
        avatarUrl,
        name,
        location,
        publicRepos,
        followers
    ).toMono()
}