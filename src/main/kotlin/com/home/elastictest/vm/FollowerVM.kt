package com.home.elastictest.vm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.home.elastictest.entities.Follower
import reactor.kotlin.core.publisher.toMono

@JsonIgnoreProperties(ignoreUnknown = true)
data class FollowerVM(
    val id: Int,
    @JsonProperty("node_id")
    val nodeId: String,
    val login: String,
    @JsonProperty("avatar_url")
    val avatarUrl: String,
    var ownerId: Int?
) {
    internal fun toDBO() = Follower(
        id, nodeId, login, avatarUrl, ownerId ?: throw NullPointerException("Owner id must be exist")
    ).toMono()
}