package com.home.elastictest

import com.home.elastictest.rest.ReactiveClient
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.test.StepVerifier

@SpringBootTest
class ApplicationTests {
    @Autowired
    private lateinit var reactiveClient: ReactiveClient

    @Test
    fun contextLoads() {
        val users = reactiveClient
            .findAllUsers()
            .doOnNext { println(it.first()) }
        StepVerifier
            .create(users)
            .expectNextMatches { it.isNotEmpty() }
            .verifyComplete()
    }
}

